import {Injectable} from "@angular/core";

@Injectable()
export class CookieService {
  getItem(cookies: any, sKey: any): string | null{
    if (!sKey) {
      return null;
    }
    return decodeURIComponent(cookies.replace(new RegExp(
      "(?:(?:^|.*;)\\s*" +
      encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") +
      "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1"
      )
    ) || null;
  }

  setItem(cookies: any, sKey: any, sValue: any, vEnd?: any, sPath?: any, sDomain?: any, bSecure?: any): string {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
      return cookies;
    }
    let sExpires: string = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    return encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires +
      (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
  }
}
