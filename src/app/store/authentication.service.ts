import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {Observable, of} from "rxjs";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, mapTo, tap} from "rxjs/operators";
import {isPlatformBrowser} from "@angular/common";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "./cookie.service";
import {APIService} from "./api.service";

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {

  private token: string;
  private refreshToken: string;  private username: string;
  private tokenExpirationDate: number;
  private readonly TOKEN: string = "TOKEN";
  private readonly TOKEN_EXPIRATION_DATE: string = "TOKEN_EXPIRATION_DATE";
  private readonly REFRESH_TOKEN: string = "REFRESH_TOKEN";

  constructor(@Inject(PLATFORM_ID) private readonly platformId: any, private readonly router: Router,
              private readonly http: HttpClient,
              private readonly transferState: TransferState,
              private readonly apiService: APIService,
              private readonly cookieManager: CookieService,
              @Inject("req") private readonly req: any) {
    if (this.req?.cookies) {
      const cookieStorage: string = cookieManager.getItem(this.req?.cookies ? this.req.cookies : document.cookie, "storage");
      if (isPlatformBrowser(this.platformId)) {
        this.setToken(cookieStorage ? JSON.parse(cookieStorage) : {});
      } else {
        const data: any = cookieStorage ? JSON.parse(cookieStorage) : {};
        this.token = data?.access_token;
      }
    }
  }

  obtainAccessToken(username: string, password: string): Observable<boolean> {
    const params: URLSearchParams = this.getParams();
    params.append("grant_type", "password");
    params.append("username", username);
    params.append("password", password);
    this.username = username;
    return this.getAndSetToken(params);
  }

  private getParams(): URLSearchParams {
    const params: URLSearchParams = new URLSearchParams();
    params.append("client_id", "SUPERSECRET");
    return params;
  }

  private getAndSetToken(params: URLSearchParams): Observable<boolean> {
    const headers: HttpHeaders =
      new HttpHeaders({"Content-type": "application/x-www-form-urlencoded; charset=utf-8"});
    const options: object = {headers};
    return this.http.post("/o/token/", params.toString(), options)
      .pipe(
        tap(tokens => {
          this.saveToken(tokens);
        }),
        mapTo(true),
        catchError(error => {
          console.error(error);
          return of(false);
        }));
  }

  saveToken(token: any): void {
    if (token) {
      this.setToken(token);
      this.router.navigate(["/"]).then();
    }
  }

  setToken(token: any): void {
    const expireDate: number = new Date().getTime() + (1000 * token.expires_in);
    this.token = token.access_token;
    this.refreshToken = token.refresh_token;
    this.tokenExpirationDate = expireDate;
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem(this.TOKEN, this.token);
      localStorage.setItem(this.TOKEN_EXPIRATION_DATE, this.tokenExpirationDate.toString());
      localStorage.setItem(this.REFRESH_TOKEN, this.refreshToken);
      this.saveInCookies("authenticatedUser", token);
    }
  }

  checkAuthenticated(): boolean {
    if (isPlatformBrowser(this.platformId)) {
      if (!this.tokenExpirationDate && !localStorage.getItem(this.TOKEN_EXPIRATION_DATE)) { return false; }
      const expDate: number =
        this.tokenExpirationDate ? this.tokenExpirationDate : +localStorage.getItem(this.TOKEN_EXPIRATION_DATE);
      if (expDate < new Date().valueOf()) { this.logout(); return false; }
      return !!this.token || !!localStorage.getItem(this.TOKEN);
    } else {
      return !!this.token;
    }
  }

  logout(): Observable<boolean> {
    const headers: HttpHeaders =
      new HttpHeaders({"Content-type": "application/x-www-form-urlencoded; charset=utf-8"});
    const options: object = {headers};
    const params: URLSearchParams = this.getParams();
    params.append("token", this.getToken());
    this.doLogoutUser();
    return this.http.post("/o/revoke_token/", params.toString(), options)
      .pipe(
        mapTo(true),
        catchError(error => {
          console.error(error);
          return of(false);
        }));
  }
  getToken(): string {
    if (isPlatformBrowser(this.platformId)) {
      return this.token ? this.token : localStorage.getItem(this.TOKEN);
    } else {
      return this.token;
    }
  }
  getRefreshToken(): string {
    if (isPlatformBrowser(this.platformId)) {
      return this.refreshToken ? this.refreshToken : localStorage.getItem(this.REFRESH_TOKEN);
    } else {
      return this.refreshToken;
    }
  }
  refreshTkn(): Observable<boolean> {
    const params: URLSearchParams = this.getParams();
    params.append("grant_type", "refresh_token");
    params.append("refresh_token", this.getRefreshToken());
    return this.getRefreshToken() ? this.getAndSetToken(params) : of(false);
  }

  private doLogoutUser(): void {
    this.token = null;
    this.tokenExpirationDate = null;
    this.refreshToken = null;
    this.username = null;
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem(this.TOKEN);
      localStorage.removeItem(this.REFRESH_TOKEN);
      localStorage.removeItem(this.TOKEN_EXPIRATION_DATE);
      document.cookie = this.cookieManager.setItem("avw", "storage", "", null, "/");
    }
    this.router.navigate(["/login"]).then();
  }

  private saveInCookies(key: any, data: any): void {
    document.cookie = this.cookieManager.setItem("avw", "storage", "", null, "/");
    document.cookie = this.cookieManager.setItem("avw", "storage", JSON.stringify(data), null, "/");
  }
}
