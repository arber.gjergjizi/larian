import {Injectable} from "@angular/core";
import {EntityCollectionService} from "@ngrx/data/src/entity-services/entity-collection-service";
import {EntityServicesBase, EntityServicesElements} from "@ngrx/data";
import {AssessmentMethodListService} from "./assessment-method-list.service";
import {Study} from "../../core/models/study";

@Injectable()
export class AmEntityServices extends EntityServicesBase {
  constructor(
    entityServicesElements: EntityServicesElements,
    public readonly assessmentMethodListService: AssessmentMethodListService,
  ) {
    super(entityServicesElements);
    this.registerEntityCollectionServices([assessmentMethodListService]);
  }
  get studyService(): EntityCollectionService<Study> {
    return this.getEntityCollectionService<Study>("Study");
  }
}
