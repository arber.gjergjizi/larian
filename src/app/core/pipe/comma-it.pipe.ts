import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "commaIt",
  pure: true
})
export class CommaItPipe implements PipeTransform {

  transform(value: string | number): string {
    if (value !== null && value !== undefined && value.toString().includes(".")) {
      const isPatternAllowed: RegExp = new RegExp(/^-?\d+(?:\.\d{0,2})?/);
      if (isPatternAllowed.test(value.toString())) {
        // @ts-ignore
        const replaceStr: string = value.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        return replaceStr.toString().replace("." , "," );
      }
    } else {
      return value === null || value === undefined ? "" : value + "";
    }
    return value + "";
  }
}
