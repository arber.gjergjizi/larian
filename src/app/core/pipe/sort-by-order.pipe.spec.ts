import {SortByOrderPipe} from "./sort-by-order.pipe";
import {Choice} from "../models/choice";

describe("[Sort by order] Pipe to order", () => {
  it("check if SortByOrderPipe can be created", () => {
    const pipe: SortByOrderPipe = new SortByOrderPipe();
    expect(pipe).toBeTruthy();
  });
  it("Sort choices by identifier", () => {
    // given
    const pipe: SortByOrderPipe = new SortByOrderPipe();
    const choice: Choice = new Choice();
    choice.identifier = "test2";
    const choice2: Choice = new Choice();
    choice2.identifier = "test1";
    const data: Array<Choice> = new Array<Choice>(choice, choice2);

    // when
    const result: Array<Choice> = pipe.transform(data, "identifier");

    // then
    expect(result.length).toBe(2);
    expect(result[0].identifier).toBe("test1");
  });

  it("return null if object is not array", () => {
    // given
    const pipe: SortByOrderPipe = new SortByOrderPipe();

    // when
    const result: Array<string> = pipe.transform({}, "");

    // then
    expect(result).toBe(null);
  });
});
