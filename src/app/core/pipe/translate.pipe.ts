import {Pipe, PipeTransform} from "@angular/core";
import {TranslateService} from "../../shared/services/translate.service";
import {Language} from "../utl/text-bundle";
import {ActivatedRoute} from "@angular/router";

@Pipe({
  name: "translate",
  pure: true
})
export class TranslatePipe implements PipeTransform {
  constructor(private readonly translationService: TranslateService, private readonly route: ActivatedRoute) { }
  transform(data: string | object, textIdentifier?: string, replacements?: Array<object>): string {
    if (data === undefined) {
      return "";
    }
    if (typeof data === "object") {
      // @ts-ignore
      return this.objectAttributeValue(data, textIdentifier);
    } else {
      if (this.route.snapshot.queryParamMap.get("showTextBundles") === "true") {
        return textIdentifier ? textIdentifier : data;
      }
      // @ts-ignore
      return this.getText(textIdentifier, data, replacements);
    }
  }

  private objectAttributeValue(data: object, textIdentifier: string): any {
    // @ts-ignore
    return this.translationService.getLanguage() === Language.de ? data[textIdentifier] ? data[textIdentifier] : data[textIdentifier + "En"] : data[textIdentifier + "En"] ? data[textIdentifier + "En"] : data[textIdentifier];
  }

  private getText(textIdentifier: string, data: string, replacements: Array<object>): string {
    // @ts-ignore
    let result: string = this.translationService.getTextBundles().has(textIdentifier) ? this.translationService.getTextBundles().get(textIdentifier)
        .label.get(this.translationService.getLanguage()) : data;
    if (replacements) {
      replacements.forEach((obj: object) => {
        result = result.replace("%" + Object.keys(obj).find(o => true) + "%",
          Object.values(obj).find(o => true));
      });
    }
    return result;
  }
}
