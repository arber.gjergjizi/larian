import {Pipe, PipeTransform} from "@angular/core";

interface IndexMap {
  term: string;
  index: number;
  endIndex: number;
}

interface EscapedString {
  org: string;
  value: string;
  shiftPositions: number[];
}

@Pipe({name: "highlight", pure: true})
export class HighlightPipe implements PipeTransform {
  transform(value: any, searchTerms: any): string {
    if (!searchTerms) {
      return value;
    }

    const compareValue: string = value.toLowerCase();

    const escapedTerms: string = this.escapeAccents(searchTerms.replace(/[*-]/g, " ")).value;
    const terms: string[] = escapedTerms.toLowerCase().split(" ")
      .filter((e: any) => e).filter( this.onlyUnique );
    const foundPositions: IndexMap[] = [];

    const compareEsacped: EscapedString = this.escapeAccents(compareValue);
    const shiftedIdxs: number[] = compareEsacped.shiftPositions;

    for (const term of terms) {
      const foundIdx: number = compareEsacped.value.indexOf(term);
      if (-1 < foundIdx) {
        foundPositions.push({
          term,
          index: foundIdx,
          endIndex: (foundIdx + term.length)
        });
      }
    }
    let returnValue: string;
    if (0 < foundPositions.length) {
      foundPositions.sort((one, other) => one.index - other.index);
      let fromIdx: number = 0;
      returnValue = "";
      const result: any = this.setPositions(foundPositions, shiftedIdxs, returnValue, value, fromIdx);
      returnValue = result.returnValue;
      fromIdx = result.fromIdx;
      if (fromIdx < value.length) {
        returnValue += value.substring(fromIdx);
      }
    } else {
      returnValue = value;
    }
    return returnValue;
  }

  private setPositions(foundPositions: Array<IndexMap>,
                       shiftedIdxs: Array<number>,
                       returnValue: string,
                       value: any,
                       fromIdx: number): any {
    let lastProcessedEndIndex: number = 0;
    for (const foundPos of foundPositions) {
      let startCut: number = foundPos.index;
      let endCut: number = foundPos.endIndex;

      if (lastProcessedEndIndex > startCut) {
        // do not overlap highlight
        continue;
      }

      for (const shiftIdx of shiftedIdxs) {
        if (startCut > shiftIdx) {
          startCut--;
          endCut--;

        } else if (startCut <= shiftIdx && endCut > shiftIdx) {
          endCut--;
        }
      }

      lastProcessedEndIndex = foundPos.endIndex;

      returnValue += value.substring(fromIdx, startCut);
      // now highlight contents
      returnValue += "<span class=\"highlight\">" + value.substring(startCut, endCut) + "</span>";
      fromIdx = endCut;
    }
    return {returnValue, fromIdx};
  }

  onlyUnique(value: any, index: any, self: any): boolean {
    return self.indexOf(value) === index;
  }
  private escapeAccents(inStr: string): EscapedString {
    const pattern: RegExp = /([àáâãå])|([ç])|([èéêë])|([ìíîï])|([ñ])|([òóôõø])|([ß])|([ùúû])|([ÿ])|([æä])|([ö])|([ü])/g;
    let positionIdx: number = 0;
    const shiftPositions: number[] = [];
    const escaped: string = inStr.replace(pattern,
      (str: string, a: string, c: string, e: string, i: string, n: string, o: string, s: string, u: string,
       y: string, ae: string, oe: string, ue: string) => {
        const foundIdx: number = inStr.indexOf(str, positionIdx);
        positionIdx = foundIdx + 1;
        if (a) {
          return "a";
        } else if (c) {
          return "c";
        } else if (e) {
          return "e";
        } else if (i) {
          return "i";
        } else if (n) {
          return "n";
        } else if (o) {
          return "o";
        } else if (s) {
          shiftPositions.push(foundIdx);
          return "ss";
        } else if (u) {
          return "u";
        } else if (y) {
          return "y";
        } else if (ae) {
          shiftPositions.push(foundIdx);
          return "ae";
        } else if (oe) {
          shiftPositions.push(foundIdx);
          return "oe";
        } else if (ue) {
          shiftPositions.push(foundIdx);
          return "ue";
        } else {
          return str;
        }
      }
    );
    return {
      org: inStr,
      value: escaped,
      shiftPositions
    };
  }

}
