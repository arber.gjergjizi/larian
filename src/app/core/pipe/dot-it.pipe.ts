import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "dotIt",
  pure: true
})
export class DotItPipe implements PipeTransform {

  transform(value: string): any {
    if (value !== null && value !== undefined && value.toString().includes(",")) {
      return value.toString().replace("," , "." );
    } else {
      return value;
    }
  }
}
