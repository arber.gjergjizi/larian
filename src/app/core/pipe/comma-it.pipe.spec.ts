import {CommaItPipe} from "./comma-it.pipe";

describe("[CommaItPipe] Pipe to convert string with dot into string with comma separation", () => {
  it("create an instance", () => {
    const pipe: CommaItPipe = new CommaItPipe();
    expect(pipe).toBeTruthy();
  });

  it("string 2,3 should return 2.3", () => {
    // given
    const pipe: CommaItPipe = new CommaItPipe();
    const stringToDotIt: string = "2.3";

    // when
    const result: string = pipe.transform(stringToDotIt);

    // then
    expect(result).toBe("2,3");
  });

  it("string .3 should return .3", () => {
    // given
    const pipe: CommaItPipe = new CommaItPipe();
    const stringToDotIt: string = ".3";

    // when
    const result: string = pipe.transform(stringToDotIt);

    // then
    expect(result).toBe(".3");
  });

  it("string . should return .", () => {
    // given
    const pipe: CommaItPipe = new CommaItPipe();
    const stringToDotIt: string = ".";

    // when
    const result: string = pipe.transform(stringToDotIt);

    // then
    expect(result).toBe(".");
  });
});
