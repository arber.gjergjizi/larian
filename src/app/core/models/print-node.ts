export class PrintNode {
  id: string | undefined;
  uuid: string | undefined;
  title: string | undefined;
  term: number | undefined;
  locationInCurriculum: Array<number> | undefined;
  color: string | undefined;
  type: string | undefined;
  position: number | undefined;
  children: Array<PrintNode> | undefined;
}
