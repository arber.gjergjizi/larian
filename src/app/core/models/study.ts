import {PermissionsAttributes} from "./permissions";

export class Study extends PermissionsAttributes {
  uuid: string | undefined;
  code: string | undefined;
  identifier: string | undefined;
  title: string | undefined;
}
