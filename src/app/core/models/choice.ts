export class Choice {
  public uuid: string | undefined;
  public identifier: string | undefined;
  public component: string | undefined;
  public choiceAbbreviationEn: string | undefined;
  public position: number | undefined;
}
