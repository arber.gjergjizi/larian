import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";
import {SlideInOutAnimation} from "../../animation";

@Component({
  selector: "am-card-group",
  templateUrl: "card-group.component.html",
  styleUrls: ["card-group.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: SlideInOutAnimation
})
export class CardGroupComponent extends UnsubscribeOnDestroy {
  @Input()
  activeTabIndex: number = 0;

  @Input()
  size: number = 0;

  @Output()
  activeTabIndexChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
    super();
  }
  onNext(): void {
    if (this.activeTabIndex !== this.size - 1) {
      this.activeTabIndex++;
      this.activeTabIndexChanged.emit(this.activeTabIndex);
    }
  }

  onPrevious(): void {
    if (this.activeTabIndex > 0) {
      this.activeTabIndex--;
      this.activeTabIndexChanged.emit(this.activeTabIndex);
    }
  }
}
