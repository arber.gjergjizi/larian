import {MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material/dialog";
import {ModalComponent} from "./modal.component";
import {filter, takeUntil} from "rxjs/operators";
import {Router} from "@angular/router";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";

export class DialogConfigBuilder extends UnsubscribeOnDestroy {
  private readonly dialogRef: MatDialogRef<ModalComponent>;
  private navigationUrl: string = "";
  private router: Router | undefined;
  constructor(message: string, matDialog: MatDialog, errorFlag: boolean= false) {
    super();
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {message, errorFlag};
    this.dialogRef = matDialog.open(ModalComponent, dialogConfig);
  }
  setNavigationUrl(url: string, router: Router): DialogConfigBuilder {
    this.navigationUrl = url;
    this.router = router;
    return this;
  }
  show(uuid: any, service: any): void {
    this.dialogRef.afterClosed().pipe(filter(o => !!o), takeUntil(this.componentDestroyed)).subscribe(o => {
        if (uuid) {
          service.delete(uuid).subscribe(() => {
            if (this.navigationUrl) { // @ts-ignore
              this.router.navigateByUrl(this.navigationUrl).then(); }
            }
          );
        }
      }
    );
  }
  getDialogRef(): any {
    return this.dialogRef;
  }
}
