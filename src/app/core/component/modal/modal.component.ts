import {ChangeDetectionStrategy, Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";

@Component({
  selector: "am-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent extends UnsubscribeOnDestroy implements OnInit {

  message: string;
  title: string;
  errorFlag: boolean = false;

  constructor(
    private readonly dialogRef: MatDialogRef<ModalComponent>,
    // tslint:disable-next-line:typedef
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    super();
    this.title = data.title;
    this.message = data.message;
    this.errorFlag = data.errorFlag;
  }
  ngOnInit(): void {}

  ok(): void {
    this.dialogRef.close(true);
  }

  cancel(): void {
    this.dialogRef.close(false);
  }

}
