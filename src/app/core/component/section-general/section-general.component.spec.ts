import { ComponentFixture, TestBed } from "@angular/core/testing";

import { SectionGeneralComponent } from "./section-general.component";
import {SectionComponent} from "../../../shared/section/section.component";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../../core.module";
import {AmStoreModule} from "../../../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "../../../shared/services/authentication.service";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "../../../shared/services/cookie.service";
import {LoaderService} from "../../../shared/services/loader.service";

describe("SectionGeneralComponent", () => {
  let component: SectionGeneralComponent;
  let fixture: ComponentFixture<SectionGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SectionComponent],
      imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
      providers: [AuthenticationService, {provide: "req", useValue: null}, TransferState, CookieService, LoaderService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
