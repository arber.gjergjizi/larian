import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {EntityCollectionService} from "@ngrx/data";
import {HelpPage} from "../../models/helppage";
import {filter, takeUntil} from "rxjs/operators";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";

@Component({
  selector: "am-section-general",
  templateUrl: "./section-general.component.html",
  styleUrls: ["./section-general.component.scss"]
})
export class SectionGeneralComponent extends UnsubscribeOnDestroy implements OnInit {
  protected readonly helpPageService: EntityCollectionService<HelpPage> | undefined;

  @Input()
  headLineText: string = "No headline text was given";

  @Input()
  helpIdentifier: string = "";

  @Output()
  submitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private readonly eRefBase: ElementRef,
              private readonly cdBase: ChangeDetectorRef
  ) { super(); }

  localOnlySave: boolean = false;
  localHelpButton: boolean = false;

  ngOnInit(): void {
    this.initState();
  }

  initState(): void {
    this.setOnlySave();
    // @ts-ignore
    this.helpPageService.entities$.pipe(filter(o => !!o), takeUntil(this.componentDestroyed))
      .subscribe(o => {
        this.localHelpButton = o.some(i => i.sectionIdentifier === this.helpIdentifier);
        this.cdBase.markForCheck();
      });
  }

  setOnlySave(): void {
  }

  submitForm(): void {
    this.submitEvent.emit(true);
    this.localOnlySave = false;
  }
}
