import {ChangeDetectorRef, Component} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";

@Component({
  selector: "am-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent extends UnsubscribeOnDestroy {
  form: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl()
  });
  loginInvalid: boolean | undefined;
  loading: boolean = false;
  private formSubmitAttempt: boolean | undefined;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly cd: ChangeDetectorRef,
  ) { super(); }

  onSubmit(): void {
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    this.loading = true;
    if (this.form.valid) {
      // @ts-ignore
      const username: any = this.form.get("username").value;
      // @ts-ignore
      const password: any = this.form.get("password").value;
    } else {
      this.formSubmitAttempt = true;
    }
  }
}
