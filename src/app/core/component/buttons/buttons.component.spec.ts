import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";

import {ButtonsComponent} from "./buttons.component";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../../core/core.module";
import {AmStoreModule} from "../../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "../services/authentication.service";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "../services/cookie.service";
import {LoaderService} from "../services/loader.service";

describe("ButtonsComponent", () => {
  let component: ButtonsComponent;
  let fixture: ComponentFixture<ButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonsComponent ],
      imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
      providers: [ AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
