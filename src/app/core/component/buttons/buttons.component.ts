import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {EntityCache} from "@ngrx/data";
import {filter, switchMap, takeUntil} from "rxjs/operators";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";


@Component({
  selector: "am-buttons",
  templateUrl: "./buttons.component.html",
  styleUrls: ["./buttons.component.scss"]
})
export class ButtonsComponent extends UnsubscribeOnDestroy implements OnInit {
  @Input()
  editing: boolean = false;
  @Input()
  textBundle: string = "";

  @Output()
  deleteEvent: EventEmitter<never> = new EventEmitter<never>();

  constructor(private readonly route: ActivatedRoute,
              private readonly cd: ChangeDetectorRef,
              private readonly store: Store<EntityCache>) { super(); }

  showButtons: boolean = true;
  ngOnInit(): void {
  }

}
