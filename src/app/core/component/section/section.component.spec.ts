import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";

import {SectionComponent} from "./section.component";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../../core/core.module";
import {AmStoreModule} from "../../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "../services/authentication.service";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "../services/cookie.service";
import {LoaderService} from "../services/loader.service";

describe("SectionComponent", () => {
  let component: SectionComponent;
  let fixture: ComponentFixture<SectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionComponent ],
      imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
      providers: [ AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
