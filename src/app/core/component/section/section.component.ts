import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList
} from "@angular/core";
import {takeUntil} from "rxjs/operators";
import {DialogConfigBuilder} from "../modal/dialog-config-builder";
import {MatDialog} from "@angular/material/dialog";
import {NgForm} from "@angular/forms";
import {SectionGeneralComponent} from "../section-general/section-general.component";
import {Iform} from "../../utl/Iform";

@Component({
  selector: "am-section",
  templateUrl: "./section.component.html",
  styleUrls: ["./section.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SectionComponent extends SectionGeneralComponent implements OnInit, OnChanges {

  @Input()
  editing: boolean = false;

  @Input()
  simplified: boolean = false;

  @Output()
  editingEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private readonly eRef: ElementRef,
              private readonly matDialog: MatDialog,
              private readonly cd: ChangeDetectorRef
  ) { super(eRef, cd); }

  localEditing: boolean = false;
  localOnlySave: boolean = false;
  localShowButtons: boolean = true;

  iWasClicked: boolean = false;
  iRequested: boolean = false;

  @ContentChildren(Iform, {descendants: true}) iForms: QueryList<Iform> | undefined;
  @ContentChildren(NgForm, {descendants: true}) ngForms: QueryList<NgForm> | undefined;

  @HostListener("dblclick")
  clickInside(): void {
    if (!this.localEditing && this.localShowButtons) {
      this.toggleEdit();
    }
  }
  ngOnChanges(): void {
    this.localEditing = this.editing;
    if (!this.localEditing) {
      this.iWasClicked = false;
    }
    this.cd.markForCheck();
  }

  ngOnInit(): void {
    this.initState();
    this.localEditing = this.editing;
    this.cd.markForCheck();
  }

  checkAndShowDialog(o: number): void {
    if (this.iWasClicked && o === 2) {
      // @ts-ignore
      if (this.iForms?.length > 0 && this.iForms?.find(i => i.dirty) || this.ngForms?.length > 0 && this.ngForms?.find(i => i.dirty)) {
        // @ts-ignore
        const message: string = this.iForms?.find(i => i.valid && i.dirty)?.valid || this.ngForms?.find(i => i.valid && i.dirty) ?
            "Möchten Sie Änderungen unter " + this.headLineText + " speichern?" :
            "Möchten Sie Änderungen unter " + this.headLineText + " verwerfen?";

        const configBuilder: DialogConfigBuilder =
          new DialogConfigBuilder(message, this.matDialog);
        configBuilder.getDialogRef().afterClosed().pipe(takeUntil(this.componentDestroyed))
          .subscribe((res: boolean) => this.changeSectionState(res) );

      } else if (this.localOnlySave || !this.localShowButtons) {
        this.editingEvent.emit(!this.editingEvent);
      } else {
        this.submitEvent.emit(true);
      }
    }
  }

  private changeSectionState(res: boolean): void {
    // @ts-ignore
    if (res && (this.iForms?.find(i => i.valid)?.valid || this.ngForms?.find(i => i.valid))) {
      this.submitEvent.emit(true);
    } else {
      this.localEditing = !res;
      this.iRequested = false;
      this.iWasClicked = true;
      if (res) {
        this.editingEvent.emit(this.localEditing);
      }
    }
  }

  toggleEdit(): void {
  }

  onKeyDown($event: any): void {
    if (!this.localEditing) { return; }
    this.handleWindowsKeyEvents($event);
  }

  handleWindowsKeyEvents($event: any): void {
    if (this.localShowButtons) {
      const charCode: string = String.fromCharCode($event.which).toLowerCase();
      if ($event.ctrlKey && charCode === "s") {
        $event.preventDefault();
        this.submitForm();
      }
      if ($event.key === "Escape") {
        $event.preventDefault();
        this.toggleEdit();
      }
    }
  }
}
