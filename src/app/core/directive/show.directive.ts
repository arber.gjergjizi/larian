import {Directive, ElementRef, Input, OnInit, Renderer2} from "@angular/core";
import {UnsubscribeOnDestroy} from "../utl/unsubscribe-on-destroy";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {EntityCache} from "@ngrx/data";

@Directive({
  selector: "[amShow]"
})
export class ShowDirective extends UnsubscribeOnDestroy implements OnInit {
  @Input("amShow") model: string | undefined;
  @Input() parent: string = "mat-form-field";
  constructor(private readonly element: ElementRef,
              private readonly renderer: Renderer2,
              private readonly route: ActivatedRoute,
              private readonly store: Store<EntityCache>, ) { super(); }

  ngOnInit(): void {
    this.setClass();
  }

  private removeClass(): void {
    const element: any = this.parent !== "" ? this.element.nativeElement.closest(this.parent) : this.element.nativeElement;
    this.renderer.removeClass(element, "hide");
  }

  private setClass(): void {
    const element: any = this.parent !== "" ? this.element.nativeElement.closest(this.parent) : this.element.nativeElement;
    this.renderer.addClass(element, "hide");
  }

}
