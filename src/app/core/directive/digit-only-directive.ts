import {Directive, ElementRef, HostListener, Input} from "@angular/core";

@Directive({
  selector: "[amDigitOnly]"
})
export class DigitOnlyDirective {
  private decimalCounter: number = 0;
  private readonly navigationKeys: Array<string> = [
    "Backspace",
    "Delete",
    "Tab",
    "Escape",
    "Enter",
    "Home",
    "End",
    "ArrowLeft",
    "ArrowRight",
    "Clear",
    "Copy",
    "Paste"
  ];
  @Input() decimal ?: boolean = false;
  inputElement: HTMLInputElement;

  constructor(public el: ElementRef) {
    this.inputElement = el.nativeElement;
  }

  @HostListener("keydown", ["$event"])
  onKeyDown(e: KeyboardEvent): void {
    if (this.skipKey(e) || this.skipKeyAddition(e)) {
      return;
    }
    if (e.key === " " || isNaN(+(e.key))) {
      e.preventDefault();
    }
  }

  @HostListener("keyup", ["$event"])
  onKeyUp(e: KeyboardEvent): void {
    if (!this.decimal) {
      return;
    } else {
      this.decimalCounter = this.el.nativeElement.value.split(",").length - 1;
    }
  }

  @HostListener("paste", ["$event"])
  onPaste(event: ClipboardEvent): void {
    // @ts-ignore
    const pastedInput: string = event.clipboardData.getData("text/plain");
    this.checkInput(pastedInput, event);
  }

  @HostListener("drop", ["$event"])
  onDrop(event: DragEvent): void {
    // @ts-ignore
    const textData: string = event.dataTransfer.getData("text");
    this.inputElement.focus();
    this.checkInput(textData, event);
  }

  private checkInput(pastedInput: string, event: ClipboardEvent | DragEvent): void {
    let pasted: boolean = false;
    if (!this.decimal) {
      pasted = document.execCommand(
        "insertText",
        false,
        pastedInput.replace(/[^0-9]/g, "")
      );
    } else if (this.isValidDecimal(pastedInput)) {
      pasted = document.execCommand(
        "insertText",
        false,
        pastedInput.replace(/[^0-9.]/g, "")
      );
    }
    if (pasted) {
      event.preventDefault();
    } else {
      if (navigator.clipboard) {
        navigator.clipboard.writeText(pastedInput).then();
        document.execCommand("paste");
      }
    }
  }

  isValidDecimal(str: string): boolean {
    return str.split(",").length <= 2;
  }

  private skipKey(e: KeyboardEvent): boolean {
    if (
      this.navigationKeys.indexOf(e.key) > -1 || (e.key === "a" && e.ctrlKey) || (e.key === "c" && e.ctrlKey) || (e.key === "v" && e.ctrlKey) || (e.key === "x" && e.ctrlKey) || (e.key === "a" && e.metaKey) || (e.key === "c" && e.metaKey) || (e.key === "v" && e.metaKey) // Allow: Cmd+V (Mac)
    ) {
      return true;
    }
    return false;
  }

  private skipKeyAddition(e: KeyboardEvent): boolean {
    if ((e.key === "x" && e.metaKey) || (e.key === "ArrowUp") || (e.key === "ArrowDown") || (e.key === "Backspace") ||
      (this.decimal && (e.key === "." || e.key === ",") && this.decimalCounter < 1) // Allow: only one decimal point
    ) {
      return true;
    }
    return false;
  }
}
