import {Directive, ElementRef, Input, OnChanges, OnInit, Renderer2} from "@angular/core";

// tslint:disable-next-line:directive-selector
@Directive({ selector: "[myFocus]" })
export class FocusDirective implements OnInit, OnChanges {

  @Input("myFocus") isFocused: boolean | undefined;

  constructor(private readonly hostElement: ElementRef, private readonly renderer: Renderer2) {}

  ngOnInit(): void {
    if (this.isFocused) {
      this.renderer.selectRootElement(this.hostElement.nativeElement).focus();
    }
  }

  ngOnChanges(): void {
    this.setFocus();
  }

  private setFocus(): void {
    if (this.isFocused) {
      setTimeout(() =>
        this.hostElement.nativeElement.focus(), 100);
    }
  }
}
