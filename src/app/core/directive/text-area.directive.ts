import {Directive, ElementRef, Inject, Input, OnChanges, OnInit, PLATFORM_ID, Renderer2} from "@angular/core";
import {isPlatformServer} from "@angular/common";

@Directive({
  selector: "[amTextArea]"
})
export class TextAreaDirective implements OnInit, OnChanges{
  @Input("amTextArea") data: string | undefined;
  parent: string = "textarea";
  constructor(private readonly renderer: Renderer2,
              private readonly element: ElementRef,
              @Inject(PLATFORM_ID) private readonly platformId: any) { }

  ngOnInit(): void {
    this.setClass();
  }
  ngOnChanges(): void {
    this.setClass();
  }
  private setClass(): void {
    if (this.data && isPlatformServer(this.platformId)) {
      const element: any = this.data !== "" ? this.element.nativeElement.closest(this.parent) : this.element.nativeElement;
      const dataArray: Array<string> = this.data.split("\n");
      let lines: number = dataArray.length + 1;
      for (const line of dataArray) {
        if (line.length > 170) {
          lines = lines + (line.length / 170);
        }
      }
      lines = lines  * 18;
      element.style.height = lines + "px";
    }
  }
}
