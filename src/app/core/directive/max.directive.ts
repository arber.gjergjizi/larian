import {Directive, Input} from "@angular/core";
import {AbstractControl, NG_VALIDATORS, Validator, Validators} from "@angular/forms";

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: "[max]",
  providers: [{ provide: NG_VALIDATORS, useExisting: MaxDirective, multi: true }]
})
export class MaxDirective implements Validator {

  @Input() max: number | undefined;

  validate(control: AbstractControl): { [key: string]: any } {
    // @ts-ignore
    return Validators.max(this.max)(control);
  }
}
