import {animate, AnimationTriggerMetadata, group, query, state, style, transition, trigger} from "@angular/animations";

export const SlideInOutAnimation: Array<AnimationTriggerMetadata> = [
  trigger("animSlider", [
    state("*", style({
      width: "100%",
    })),
    transition(":increment", [
      query(":enter, :leave", style({ width: "100%"}), { optional: true }),
      group([
        query(":enter", [style({ transform: "translateX(100%)", "-webkit-transform": "translateX(100%)" }), animate(".3s ease-out" )], {
          optional: true,
        }),
        query(":leave", [style({ display: "none"})], {
          optional: true,
        }),
      ]),
    ]),
    transition(":decrement", [
      query(":enter, :leave", style({ width: "100%"  }), { optional: true }),
      group([
        query(":enter", [style({ transform: "translateX(-100%)",  "-webkit-transform": "translateX(-100%)" }), animate(".3s ease-out")], {
          optional: true,
        }),
        query(":leave", [style({ display: "none" })], {
          optional: true,
        }),
      ]),
    ]),
  ]),
];
