import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthenticationGuard implements CanActivate {
  constructor(public authService: AuthenticationService, public router: Router) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.checkAuthenticated()) {
      this.router.navigate(["login"]).then();
      return false;
    }
    return true;
  }

}
