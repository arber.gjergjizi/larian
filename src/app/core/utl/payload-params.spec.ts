import {PayloadParams} from "./payload-params";
import {Course} from "../models/course";

describe("PayloadParams", () => {
  it("should create an instance", () => {
    expect(new PayloadParams()).toBeTruthy();
  });
  it("should return merged object", () => {
    // given
    const pp: PayloadParams = new PayloadParams();
    pp.studyUuid = "123456";
    const course: Course = new Course();
    course.uuid = "666";

    // when
    const result: object = pp.merge(course, pp);

    // then
    expect(typeof result === "object").toBeTrue();
    expect(result.hasOwnProperty("studyUuid")).toBeTruthy();
    expect(result[`studyUuid`]).toBe("123456");
    expect(result[`uuid`]).toBe("666");
  });
});
