export function shallowCopy<T>(object: T): T {
  if ( Array.isArray(object)) {
    return Object.assign([], object);
  } else {
    return Object.assign({}, object);
  }
}

// tslint:disable-next-line:typedef
export const deepCopy = <T>(target: T): T => {
  if (target === null) {
    return target;
  }
  if (target instanceof Date) {
    return new Date(target.getTime()) as any;
  }
  if (target instanceof Array) {
    const cp: any[] = [] as any[];
    (target as any[]).forEach(v => { cp.push(v); });
    return cp.map((n: any) => deepCopy<any>(n)) as any;
  }
  if (typeof target === "object" && target !== {}) {
    const cp: {} = { ...(target as { [key: string]: any }) } as { [key: string]: any };
    Object.keys(cp).forEach(k => {
      // @ts-ignore
      cp[k] = deepCopy<any>(cp[k]);
    });
    return cp as T;
  }
  return target;
};

export function distinctBy<T, U extends string | number>(array: T[], mapFn: (el: T) => U): T[] {
  const uniqueKeys: Set<U> = new Set(array.map(mapFn));
  return array.filter(el => uniqueKeys.has(mapFn(el)));
}
