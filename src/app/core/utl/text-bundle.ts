export class TextBundles {
  textBundles: Map<string, Label>;
  constructor() {
    this.textBundles = new Map<string, Label>();
  }
}

export class Label {
  label: Map<Language, string>;
  constructor() {
    this.label = new Map<Language, string>();
  }
}

export enum Language {
  en,
  de
}

export class TextBundle {
  textIdentifier: string;
  lab: Label;

  constructor(textIdentifier: string, de: string, en: string = "") {
    this.textIdentifier = textIdentifier;
    const labelValue: Label = new Label();
    if (de) {
      labelValue.label.set(Language.de, de);
    }
    if (en) {
      labelValue.label.set(Language.en, en);
    }
    this.lab = labelValue;
  }
}
