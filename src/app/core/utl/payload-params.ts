type OptionalPropertyNames<T> = { [K in keyof T]: undefined extends T[K] ? K : never }[keyof T];
type SpreadProperties<L, R, K extends keyof L & keyof R> = { [P in K]: L[P] };
type Id<T> = {[K in keyof T]: T[K]};
type Spread<L, R> = Id<& Pick<L, Exclude<keyof L, keyof R>> & Pick<R, Exclude<keyof R, OptionalPropertyNames<R>>>
  & Pick<R, Exclude<OptionalPropertyNames<R>, keyof L>> & SpreadProperties<L, R, OptionalPropertyNames<R> & keyof L>>;

export class PayloadParams {
  [key: string]: string | boolean | number;

  // @ts-ignore
  public merge<A extends object, B extends object>(a: A, b: B): Spread<A, B> {
    return Object.assign({}, a, b) as Spread<A, B>;
  }
}
