import {ActivatedRouteSnapshot} from "@angular/router";

export class ResolveActivatedRoute {
  getResolvedUrl(route: ActivatedRouteSnapshot): string {
    return route.pathFromRoot
      // tslint:disable-next-line:typedef
      .map(v => v.url.map(segment => segment.toString()).join("/"))
      .join("/");
  }
}
