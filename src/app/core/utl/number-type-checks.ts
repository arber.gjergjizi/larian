export class NumberTypeChecks {
  isNumber(value: string | number): boolean {
    if (value !== null && value !== undefined) {
      const isPatternAllowed: RegExp = new RegExp(/^[+-]?\d+(\.\d+)?$/);
      return isPatternAllowed.test(value.toString());
    } else {
      return false;
    }
  }
}
