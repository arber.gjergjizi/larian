import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DotItPipe} from "./pipe/dot-it.pipe";
import {CommaItPipe} from "./pipe/comma-it.pipe";
import {SortByOrderPipe} from "./pipe/sort-by-order.pipe";
import {CardSectionComponent} from "./component/card-section/card-section.component";
import {FocusDirective} from "./directive/focus.directive";
import {CommonModule} from "@angular/common";
import {MatSelectModule} from "@angular/material/select";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatButtonModule} from "@angular/material/button";
import {MatChipsModule} from "@angular/material/chips";
import {MatRadioModule} from "@angular/material/radio";
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatCardModule} from "@angular/material/card";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatTabsModule} from "@angular/material/tabs";
import {MatNativeDateModule} from "@angular/material/core";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatListModule} from "@angular/material/list";
import {MatSliderModule} from "@angular/material/slider";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatTableModule} from "@angular/material/table";
import {MatStepperModule} from "@angular/material/stepper";
import {DigitOnlyDirective} from "./directive/digit-only-directive";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {MatTreeModule} from "@angular/material/tree";
import {MinDirective} from "./directive/min.directive";
import {MaxDirective} from "./directive/max.directive";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {LoginComponent} from "./component/login/login.component";
import {MatPaginatorModule} from "@angular/material/paginator";
import {CardGroupComponent} from "./component/card-group/card-group.component";
import {ShowDirective} from "./directive/show.directive";
import {ContentDirective} from "./directive/content.directive";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {SectionGeneralComponent} from "./component/section-general/section-general.component";
import {TextAreaDirective} from "./directive/text-area.directive";
import {TranslatePipe} from "./pipe/translate.pipe";
import {SectionComponent} from "./component/section/section.component";
import {ButtonsComponent} from "./component/buttons/buttons.component";
import {ModalComponent} from "./component/modal/modal.component";
import {MatDialogModule} from "@angular/material/dialog";

@NgModule({
  imports: [
    RouterModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatChipsModule,
    MatRadioModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatTabsModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatGridListModule,
    MatTableModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    MatStepperModule,
    DragDropModule,
    MatTreeModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatDialogModule
  ],
  declarations: [
    DotItPipe,
    CommaItPipe,
    TranslatePipe,
    SortByOrderPipe,
    DigitOnlyDirective,
    SectionComponent,
    CardSectionComponent,
    FocusDirective,
    MinDirective,
    MaxDirective,
    LoginComponent,
    CardGroupComponent,
    ShowDirective,
    ButtonsComponent,
    ContentDirective,
    SectionGeneralComponent,
    ModalComponent,
    TextAreaDirective
  ],
  exports: [
    RouterModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatChipsModule,
    MatRadioModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatTabsModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatGridListModule,
    MatTableModule,
    ReactiveFormsModule,
    DragDropModule,
    FormsModule,
    MatTreeModule,
    DotItPipe,
    CommaItPipe,
    TranslatePipe,
    ModalComponent,
    DigitOnlyDirective,
    SectionComponent,
    CardSectionComponent,
    FocusDirective,
    MinDirective,
    MaxDirective,
    CommonModule,
    MatStepperModule,
    SortByOrderPipe,
    MatDatepickerModule,
    MatPaginatorModule,
    CardGroupComponent,
    ShowDirective,
    ButtonsComponent,
    ContentDirective,
    SectionGeneralComponent,
    TextAreaDirective
  ],
  providers: [
    CommaItPipe,
    TranslatePipe,
    SortByOrderPipe]
})
export class CoreModule {}
