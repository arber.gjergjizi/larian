import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {ProductListComponent} from "./product-list/product-list.component";

const studyRoutes: Routes = [
  { path: "", component: ProductListComponent},
  ];

@NgModule({
  imports: [RouterModule.forChild(studyRoutes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
