import {NgModule} from '@angular/core';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductRoutingModule} from "./product-routing.module";
import {CoreModule} from "../core/core.module";

@NgModule({
  declarations: [
    ProductListComponent
  ],
  imports: [
    CoreModule,
    ProductRoutingModule
  ]
})
export class ProductModule { }
