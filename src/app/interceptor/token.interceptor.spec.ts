import {TransferState} from "@angular/platform-browser";
import {TestBed} from "@angular/core/testing";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../core/core.module";
import {AmStoreModule} from "../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "./services/authentication.service";
import {CookieService} from "./services/cookie.service";
import {LoaderService} from "./services/loader.service";
import {TokenInterceptor} from "./token.interceptor";

describe("token interceptor", () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
    providers: [ AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService,
      TokenInterceptor]
  }));
  it("should be created", () => {
    const tokenInterceptor: TokenInterceptor = TestBed.inject(TokenInterceptor);
    expect(tokenInterceptor).toBeTruthy();
  });
});
