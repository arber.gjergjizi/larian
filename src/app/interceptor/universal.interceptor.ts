import {Inject, Injectable, Optional} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Request} from "express";
import {REQUEST} from "@nguniversal/express-engine/tokens";
import {Observable} from "rxjs";
import {makeStateKey, TransferState} from "@angular/platform-browser";
import {tap} from "rxjs/operators";
import {environment} from "../../environments/environment.prod";

@Injectable()
export class UniversalInterceptor implements HttpInterceptor {

  constructor(@Optional() @Inject(REQUEST) protected request: Request, private readonly transferState: TransferState) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let serverReq: HttpRequest<any> = req;
    if (this.request) {
      let newUrl: string = `${this.request.protocol}://${environment.APIUrl + ":" + environment.APIPort}`;
      if (!req.url.startsWith("/")) {
        newUrl += "/";
      }
      newUrl += req.url;
      serverReq = req.clone({url: newUrl});
    }
    return next.handle(serverReq).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          this.transferState.set(makeStateKey(req.url + req.params.toString()), event.body);
        }
      })
    );
  }
}
