import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {Label, Language, TextBundles} from "../../core/utl/text-bundle";
import {APIService} from "./api.service";
import {isPlatformBrowser} from "@angular/common";
import {of} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TranslateService {
  private readonly data: TextBundles = new TextBundles();
  private language: Language = Language.de;

  constructor(@Inject(PLATFORM_ID) private readonly platformId: any, private readonly apiService: APIService) { }

  public getTextBundles(): Map<string, Label> {
    return this.data.textBundles;
  }

  getConfigs(): Promise<any> {
    return this.apiService.get<Array<TextbundleBackend>>("/api/textbundles")
      .toPromise()
      .then(resp => {
        resp.forEach(o => {
          const labelValue: Label = new Label();
          labelValue.label.set(Language.de, <string>o.de);
          labelValue.label.set(Language.en, <string>o.en);
          this.data.textBundles.set(<string>o.identifier, labelValue);
        });
      }).catch(error => {
        console.log(error);
        return of(true).toPromise();
      });
  }

  setLanguage(language: Language): void {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem("language", language.toString());
    }
    this.language = language;
  }
  getLanguage(): Language {
    let lang: string | null = null;
    if (isPlatformBrowser(this.platformId)) {
      lang = localStorage.getItem("language");
    }
    if (lang && lang === "0") {
      this.language = Language.en;
    }
    return this.language;
  }
}

export class TextbundleBackend {
  uuid: string | undefined;
  identifier: string | undefined;
  en: string | undefined;
  de: string | undefined;
}
